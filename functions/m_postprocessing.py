"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    
    vec_accel = []

    for i, j in enumerate(x):
        #Betrag der Beschleunigung für jeden Eintrag bestimmen
        vec_accel_i = np.sqrt(x[i]*x[i]+ y[i]*y[i] + z[i]*z[i])
        vec_accel.append(vec_accel_i)
        
    return vec_accel

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    x_interp = np.linspace(np.min(time), np.max(time), len(time))
    
    yinterp = np.interp(x_interp, time, data)
    
    return x_interp, yinterp

def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    X = fft(x)
    N = len(X)
    n = np.arange(N)
    sr = len(x)/(np.max(time)-np.min(time))
    T = N / sr
    #Einseitige Berechnung aufgrund der Symmetrie
    n_oneside = int(np.ceil(N / 2))
    #Frequenz und Amplitude berechnen
    Frequency = n/T
    Frequency_oneside = Frequency[:n_oneside]
    Amplitude = np.abs(X)
    Amplitude_oneside = Amplitude[:n_oneside]

    
    return (Amplitude_oneside, Frequency_oneside)